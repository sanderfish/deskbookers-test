import React, { Component } from 'react';
import AppHeader from './js/AppHeader/AppHeader';
import Slider from './js/Slider/Slider';

// include slider images
const reqImgs = require.context("./img/", false, /sliderimage/);
const sliderImages = reqImgs.keys().map(reqImgs);

/*
	CSS
*/

import './css/searchresult.css';

export default class DeskbookersApp extends Component {
  render() {
    return (
      <div className="DeskbookersApp">
				<AppHeader />
				<Slider images={ sliderImages } isInfinite={true} delay={5000}/>
      </div>
    );
  }
}
