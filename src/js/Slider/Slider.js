/*
	Slider
	<Slider/>
*/

import React from 'react';
import SearchContainer from './../Search/SearchContainer';
import '../../css/slider.css';

export default class Slider extends React.Component {
  constructor(props) {
    super(props);

    this.previousItem = this.previousItem.bind(this);
    this.nextItem = this.nextItem.bind(this);
    this.updatePosition = this.updatePosition.bind(this);
    this.setVisibleItems = this.setVisibleItems.bind(this);
    this.sliderStyle = this.sliderStyle.bind(this);
    this.isVisible = this.isVisible.bind(this);

    this.state = {
      images: [],
      currentPosition: 0,
			visibleItems: 1,
    };
  }

  componentDidMount() {
    this.setVisibleItems(this.props.visibleItems);
		window.addEventListener('resize', this.setVisibleItems.bind(this, this.props.visibleItems));
  }

  componentWillMount() {
    const images = (this.props.images).map((image, count) => {
      return image;
    });
    this.setState({images});
  }

	componentWillUnmount() {
		window.addEventListener('resize', this.setVisibleItems.bind(this, this.props.visibleItems));
	}

  previousItem() {
    this.updatePosition(this.state.currentPosition - 1);
  }

  nextItem() {
    this.updatePosition(this.state.currentPosition + 1);
  }

  updatePosition(position) {
    const whole = position + this.state.visibleItems;

    if (position < 0) {
      this.setState({ currentPosition: whole });
    }

    if (whole > this.state.images.length) {
      this.setState({ currentPosition: 0 });
    }

    if (whole > this.state.images.length || position < 0) {
      return;
    }

    this.setState({ currentPosition: position });
  }

  setVisibleItems() {
    this.forceUpdate();
  }

	calculateShift(offset, amount) {
    return offset * amount;
  }

  sliderStyle(classname) {
    const items = document.getElementsByClassName(classname);
    const itemWidth = (items[0]) ? items[0].offsetWidth : 0;
    const shift = this.calculateShift(itemWidth, this.state.currentPosition);
    const transform = `translateX(-${shift}px)`;

    return { transform };
  }

  isVisible(key) {
    const nextPosition = this.state.visibleItems + this.state.currentPosition;
    const visible = this.state.images.slice(this.state.currentPosition, nextPosition);

    return visible.indexOf(this.state.images[key]) !== -1;
  }

  render() {
    const sliderStyle = this.sliderStyle('slider__item');
    const { images } = this.state;
		const source = "https://www.deskbookers.com/nl-nl/sajax.json?q=Amsterdam&type=-&people=any&favorite=0&pid=&sw=52.293753%2C4.634942&ne=52.455562%2C5.162286&ids=17201%2C19640%2C13692%2C13691%2C12136%2C17938%2C15292%2C14886%2C14885%2C14884%2C14883%2C15730%2C15353%2C15351%2C15330%2C15080%2C17290%2C15454%2C15451%2C15379";

    return (
      <div className="slider__container">

        <div className="slider" style={sliderStyle}>
          {images.map((item, key) => {
            return (
							<div className='slider__item' key={key} style={{'flex': `0 0 100%`}}>
              	<div style={{backgroundImage: 'url(' + item + ')'}} className="slider__item-img">
									<div className="slider__item-content">
										<h1>Fast, Easy, Flexible</h1>
										<h3>Book one of our spaces in Amsterdam</h3>
         					</div>
               	</div>
							</div>
						)
          })}
        </div>

				<div>
					<div className="slider__navigation slider__navigation-left slider__arrow-left" onClick={this.previousItem}></div>
					<div className="slider__navigation slider__navigation-right slider__arrow-right" onClick={this.nextItem}></div>
				</div>

				<SearchContainer source={ source }/>

      </div>
    );
  }
}
