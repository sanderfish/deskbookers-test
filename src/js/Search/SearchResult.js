/*
	SearchResult
	<SearchResult/>
*/

import React from 'react';

export class SearchResult extends React.Component {
	render() {
		if (this.props.data.rating) {
			var roundRating = Math.round(this.props.data.rating * 10) / 10;
			var rating = 'Rating: ' + roundRating;
		}

		if (this.props.data.day_price) {
			var dayPrice = 'Day: €' + this.props.data.day_price;
		}

		if (this.props.data.hour_price) {
			var hourPrice = 'Hour: €' + this.props.data.hour_price;
		}

		return (
			<div className="searchresult overlay" style={{backgroundImage: 'url(' + this.props.data.image_urls[0] + ')'}}>
				<span className="searchresult__detail searchresult__name searchresult__detail-topleft">
       		<h2>{ this.props.data.name }</h2>
       	</span>
				<span className="searchresult__detail searchresult__locationname searchresult__detail-topright">
       		{ this.props.data.location_name }
       	</span>
				<span className="searchresult__detail searchresult__rating searchresult__detail-bottom">
					{ rating }
       	</span>
				<span className="searchresult__detail searchresult__dayprice searchresult__detail-bottomleft">
       		{ dayPrice }
       	</span>
				<span className="searchresult__detail searchresult__hourprice searchresult__detail-bottomright">
       		{ hourPrice }
       	</span>
   		</div>
		)
	}
}
