/*
	SearchBar
	<SearchBar />
*/


import React from 'react';
import { SearchResult } from './SearchResult';
import '../../css/search.css';

export default class SearchBar extends React.Component {

	constructor() {
		super();
		this.searchItems = this.searchItems.bind(this);
		this.state = {
			searchString: ''
		}
	}

	searchItems(e) {
		this.setState({
			searchString: e.target.value,
		});
	}

	preventFormSubmit(e) {
		const searchResultContainer = document.getElementById('searchresults');

		if (e.keyCode === 13) {
			e.preventDefault();
			searchResultContainer.scrollIntoView({behavior: "smooth"});
			return false;
		}
	}

	scrollToResults() {
		const searchResultContainer = document.getElementById('searchresults');
		searchResultContainer.scrollIntoView({behavior: "smooth"});
	}

  render() {
		var workplaces = this.props.data;
    var searchString = this.state.searchString.trim().toLowerCase();

    if (searchString.length > 0) {
			// filter results to search string, match for name or location name
      workplaces = workplaces.filter(function(w) {
        return (
					w.name.toLowerCase().match(searchString) || w.location_name.toLowerCase().match(searchString)
				)
      });
    }

		if (workplaces) {
			// when workplaces props exist, map them
			var searchWorkplaces = workplaces.map((workplace, _key) => {
				return (
					<SearchResult data={workplace} key={_key} />
				)
			})
		}

    return (
			<div>
				<div className="searchfield__wrapper">
					<form className="searchfield__form">
						<input type="text" className="searchfield" value={ this.state.searchString } onChange={ this.searchItems } onKeyDown={ this.preventFormSubmit } placeholder="Search on name or location" autoFocus="true" />
						<label onClick={ this.scrollToResults }>
							<svg className="searchfield__icon" xmlns="http://www.w3.org/2000/svg" viewBox="3 -4 20 20"><path fill="#999" d="M23.416 15.585l-6.9-6.9c1-1.3 1.6-2.8 1.6-4.5 0-4-3.3-7.3-7.3-7.3s-7.4 3.2-7.4 7.2 3.3 7.3 7.3 7.3c1.7 0 3.3-.6 4.6-1.6l6.9 6.9c.2.2.4.2.6.2.2 0 .4-.1.6-.2.3-.3.3-.8 0-1.1zm-12.7-5.8c-3.1 0-5.6-2.5-5.6-5.7 0-3.1 2.5-5.7 5.6-5.7s5.7 2.6 5.7 5.7c0 3.2-2.6 5.7-5.7 5.7z"/></svg>
						</label>
					</form>
				</div>
				<div className="searchresult__container" id="searchresults">
					<h2 className="searchresult__title">All spaces in Amsterdam</h2>
					{ searchWorkplaces }
				</div>
   		</div>
		)
  }
}
