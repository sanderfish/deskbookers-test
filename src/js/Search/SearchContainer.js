/*
	SearchResultsGet
	<SearchResultsGet/>
*/

import React from 'react';
import SearchBar from './SearchBar';

export default class SearchContainer extends React.Component {
	constructor() {
		super();

		this.state = {
			data: [],
		};
	}

	componentDidMount() {
		this.serverRequest = $.get(this.props.source, function(result) {
			this.setState({
				data: result.rows,
			});
		}.bind(this));
	}

	componentWillUnmount() {
		this.serverRequest.abort();
	}

	render() {
		const workplaces = this.state.data;
		return (
			<SearchBar data={workplaces} />
		)
	}

}
