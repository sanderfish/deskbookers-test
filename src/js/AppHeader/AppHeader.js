/*
	AppHeader
	<AppHeader/>
*/

import React from 'react';
import AppHeaderLogo from './AppHeaderLogo';
import '../../css/appheader.css';

export default class AppHeader extends React.Component {
	render() {
		return (
			<header className="appheader">
				<AppHeaderLogo />
   		</header>
		)
	}
}
