/*
	AppHeaderLogo
	<AppHeaderLogo/>
*/

import React from 'react';
import logo from '../../img/logo.png';

export default class AppHeaderLogo extends React.Component {
	render() {
		return (
			<a href="/">
				<img src={ logo } className="appheader__logo" alt="Logo" />
			</a>
		)
	}
}
