This is my interpretation of the Front-end test for Deskbookers (<a href="https://github.com/deskbookers/frontend-test">https://github.com/deskbookers/frontend-test</a>).
<br><br>

I decided to try and make the test in React (hoping for some bonus points).
<br><br>

<b>Demo can be found here: <a href="http://fishhq.nl/deskbookers-test">http://fishhq.nl/deskbookers-test</a></b>

<br>

I used Facebook's create-react-app to kickstart the development environment and not having to worry about build setup.
<br>
The app can be ran by running `npm start` or build by running `npm run build`.
<br><br>

App logic can be found in /src
<br><br>

I decided not to use Sass because create-react-app doesn't have out of the box Sass support, which means that it would take longer to set up than the time Sass would actually save. For the record: I do normally use Sass.


<h1>The Slider</h1>
I think it is better practice to not make a slider (or hero unit in general) full screen. Leaving a part of the rest of the page in sight triggers the user to scroll down or shows the user that it is actually possible to scroll down.
Not ever having built a slider in React before, I am going to admit that I used some slider logic from a JsFiddle example.
<br>
If I would rebuild the slider I think I would apply the translateX on the slider images instead of on the container. I think that would make it possible to make it infinite instead of scrolling back to the first slide after the last.


<h1>The Search bar</h1>
I think that it would be bad practice to require the user to fill in a search term instead of being able to look at the possible results. The user might not know what they are looking for yet.


<h1>The Search results</h1>
I decided to show the results straight away and filter them according to the user's input. I left out prices or ratings that weren't available.
<br>
Better practice would be to show that the rating or price is not available for the particular location.
<br>
Bonus points would have been to convert the rating into stars.

<br><br>

© Sander Visser - September 2016
